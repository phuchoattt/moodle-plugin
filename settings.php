<?php
/**
 * Admin settings for the Language Confidence question type.
 *
 * @package   qtype_languageconfidence
 * @copyright 2021 Language Confidence
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    global $CFG;

    // Default settings for audio.
    $settings->add(new admin_setting_heading('audiooptionsheading',
        get_string('optionsforaudio', 'qtype_languageconfidence'), ''));

    // Recording time limit.
    $settings->add(new admin_setting_configduration('qtype_languageconfidence/timelimit',
        get_string('timelimit', 'qtype_languageconfidence'), get_string('timelimit_desc', 'qtype_languageconfidence'),
        600, 60));

    // Audio bitrate.
    $settings->add(new admin_setting_configtext('qtype_languageconfidence/audiobitrate',
        get_string('audiobitrate', 'qtype_languageconfidence'), get_string('audiobitrate_desc', 'qtype_languageconfidence'),
        128000, PARAM_INT, 8));

    // Phone format
    $settings->add(new admin_setting_heading('reportoptionsheading', get_string('optionsforreport', 'qtype_languageconfidence'), ''));

    $settings->add(new admin_setting_configcheckbox(
        'qtype_languageconfidence/ipaformat',
        get_string('useipaformat', 'qtype_languageconfidence'),
        get_string('useipaformat_desc', 'qtype_languageconfidence'),
        1
    ));

    //other settings
    $settings->add(new admin_setting_heading('other_settings_heading', get_string('other_settings_heading', 'qtype_languageconfidence'), ''));
    $settings->add(new admin_setting_configtext('qtype_languageconfidence/company_id', get_string('company_id', 'qtype_languageconfidence'), '', $CFG->wwwroot, PARAM_TEXT));
}
