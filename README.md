# Language Confidence - Moodle Question Type Plugin

## Installation

1. Ensure you have Moodle 3.9 (2020061500) or newer installed
2. On your Moodle server copy this folder to `/path/to/moodle/question/type/languageconfidence/`
3. Sign up to our RapidAPI portal listing [here](https://rapidapi.com/language-confidence-language-confidence-default/api/pronunciation-assessment1) and get your API key
3. Edit your Moodle `config.php` file and add the following config variable:
    - `$CFG->languageconfidence_apikey`
4. Visit your Moodle admin page
5. Run the database upgrade to install the plugin

## Usage

1. Create a new Quiz activity or edit an existing Quiz activity's settings
1. Select `Immediate Feedback` for question behaviour so that students immediately see the score from the Language Confidence API before submitting their response
1. Add a new question
1. Select the `Language Confidence` question type
1. Enter the desired English phrase in the `Speech phrase` text box
1. Fill out the `Question` text box with either text or an audio recording that the student should say out loud
1. Save the question
